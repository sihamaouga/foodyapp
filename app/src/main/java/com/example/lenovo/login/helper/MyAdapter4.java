package com.example.lenovo.login.helper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lenovo.login.Order;
import com.example.lenovo.login.R;
import com.example.lenovo.login.RestoSide;

import java.util.ArrayList;

public class MyAdapter4 extends RecyclerView.Adapter<MyViewHolderRestoSide> {

    Context c;
    ArrayList<RestoSide> spacecrafts;

    public MyAdapter4(Context c, ArrayList<RestoSide> spacecrafts){
        this.c = c;
        this.spacecrafts = spacecrafts;

    }

    @NonNull
    @Override
    public MyViewHolderRestoSide onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v=LayoutInflater.from(c).inflate(R.layout.resto_side,viewGroup,false);
        return new MyViewHolderRestoSide(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderRestoSide holder, int i) {
        holder.client.setText("Client : " +spacecrafts.get(i).getClient());
        holder.adresse.setText("@Client : " +spacecrafts.get(i).getAdresse());
        holder.menu.setText("Menu : " +spacecrafts.get(i).getMen());
        holder.qt.setText("Quantite : " +spacecrafts.get(i).getQt());
        holder.prix.setText("Prix : " +spacecrafts.get(i).getPrix());

    }

    @Override
    public int getItemCount() {
        return spacecrafts.size();
    }
}
