package com.example.lenovo.login.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.example.lenovo.login.Order;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteAssetHelper {
     private static final String dbname = "Eatit.db";
     private static final int dbver = 1;
    public Database(Context context) {
        super(context, dbname, null, dbver);
    }

    public ArrayList<Order> getCarts(){
      SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlselect = {"rsto","menu","qt","prix","discount"};
        String sqlTable = "OrderDet";
        qb.setTables(sqlTable);
        Cursor c = qb.query(db,sqlselect,null,null,null,null,null);

        final ArrayList<Order> result = new ArrayList<>();
        if(c.moveToFirst()){
            do{
                result.add(new Order(c.getString(c.getColumnIndex("rsto")),c.getString(c.getColumnIndex("menu")),
                        c.getString(c.getColumnIndex("qt")),c.getString(c.getColumnIndex("prix")),c.getString(c.getColumnIndex("discount"))));
            }while (c.moveToNext());
        }
        return result;
    }

    public void addToCart(Order order){
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("INSERT INTO OrderDet(rsto,menu,qt,prix,discount) VALUES('%s','%s','%s','%s','%s');",
                order.getResto(),order.getMenu(),order.getQt(),order.getPrix(),order.getDiscount());
       db.execSQL(query);
    }

    public void cleanCart(){
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM OrderDet");
        db.execSQL(query);
    }

    public void deleteRow(Order order){
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM OrderDet WHERE rsto='%s' AND menu='%s' AND qt='%s' AND prix='%s' ; ",
                order.getResto(),order.getMenu(),order.getQt(),order.getPrix());
        db.execSQL(query);
    }
}
