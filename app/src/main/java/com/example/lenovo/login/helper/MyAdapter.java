package com.example.lenovo.login.helper;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.constraint.Constraints;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.toolbox.HttpClientStack;
import com.bumptech.glide.Glide;
import com.example.lenovo.login.Interface.ItemClickListener;
import com.example.lenovo.login.MainActivity;
import com.example.lenovo.login.MenuList;
import com.example.lenovo.login.R;
import com.example.lenovo.login.Restaurant;
import com.example.lenovo.login.RestoList2;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyViewHolderResto>{
    static Integer[] thumbnails = {
            R.drawable.quick,
            R.drawable.mcdo,
            R.drawable.king,
            R.drawable.shs,
            R.drawable.lyon,
            R.drawable.nice,
            R.drawable.starbucks,
            R.drawable.abtalcham,
            R.drawable.kfc,
            R.drawable.pizzahut,
            R.drawable.yokasushi,
            R.drawable.metros,
            R.drawable.zumba
     };
    Context c;
    ArrayList<Restaurant> spacecrafts;


    public MyAdapter(Context c, ArrayList<Restaurant> spacecrafts) {
        this.c = c;
        this.spacecrafts = spacecrafts;

    }
    @NonNull
    @Override
    public MyViewHolderResto onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v=LayoutInflater.from(c).inflate(R.layout.resto_item,viewGroup,false);
        return new MyViewHolderResto(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderResto holder, int position) {
    holder.name.setText(spacecrafts.get(position).getNamee());



      holder.image.setImageResource(thumbnails[spacecrafts.get(position).getImage()]);

       holder.setItemClickListener(new ItemClickListener() {
           @Override
           public void onClick(View view, int position, boolean isClick) {
               Toast.makeText(c,"Vous avez choisi "+spacecrafts.get(position).getNamee(),Toast.LENGTH_SHORT).show();

               Intent intent = new Intent(c, MenuList.class);
               intent.putExtra("resto", spacecrafts.get(position).getNamee());

               c.startActivity(intent);// send to restolist 3
               //finish();
              // Intent intent2 = new Intent(c, MenuList.class);

              // c.startActivity(intent2);
           }
       });
    }

    @Override
    public int getItemCount() {

        return spacecrafts.size();
       }

}
