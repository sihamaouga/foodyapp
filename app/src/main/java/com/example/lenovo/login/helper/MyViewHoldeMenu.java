package com.example.lenovo.login.helper;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.lenovo.login.Interface.ItemClickListener;
import com.example.lenovo.login.R;

public class MyViewHoldeMenu extends RecyclerView.ViewHolder implements View.OnClickListener{
    TextView menu;
    TextView price;
    ElegantNumberButton numberButton ;
    ImageButton ib;

   // ImageView image;
    private ItemClickListener itemClickListener;
    public MyViewHoldeMenu(View itemView) {
        super(itemView);
        menu=(TextView) itemView.findViewById(R.id.food);
        price = (TextView) itemView.findViewById(R.id.price) ;
        numberButton = (ElegantNumberButton) itemView.findViewById(R.id.number_button);
   //     ib = (ImageButton) itemView.findViewById(R.id.imageButton);
     //   image=(ImageView) itemView.findViewById(R.id.imageur);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}