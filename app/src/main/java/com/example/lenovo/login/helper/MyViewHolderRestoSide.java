package com.example.lenovo.login.helper;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.login.Interface.ItemClickListener;
import com.example.lenovo.login.R;

public class MyViewHolderRestoSide extends RecyclerView.ViewHolder {
    TextView client, adresse,menu, qt, prix;

    private ItemClickListener itemClickListener;

    public MyViewHolderRestoSide(View itemView) {
        super(itemView);
       client = (TextView) itemView.findViewById(R.id.cli);
       adresse =(TextView) itemView.findViewById(R.id.adr);
       menu = (TextView) itemView.findViewById(R.id.men);
       qt =(TextView) itemView.findViewById(R.id.qt);
       prix =  (TextView) itemView.findViewById(R.id.pri);
    }


}