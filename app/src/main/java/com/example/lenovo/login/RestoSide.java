package com.example.lenovo.login;

public class RestoSide {
    public String client;
    public String men;
    public String qt;
    public String prix;
    public String adresse;

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public RestoSide(String client, String adresse, String men, String qt, String prix){
        this.client = client;
        this.men = men;
        this.qt = qt;
        this.prix = prix;
        this.adresse = adresse;

        }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getMen() {
        return men;
    }

    public void setMen(String men) {
        this.men = men;
    }

    public String getQt() {
        return qt;
    }

    public void setQt(String qt) {
        this.qt = qt;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }
}
