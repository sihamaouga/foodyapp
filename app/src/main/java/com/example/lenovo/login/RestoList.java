package com.example.lenovo.login;

import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
;import com.example.lenovo.login.helper.FirebaseHelperResto;
import com.example.lenovo.login.helper.MyAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class RestoList extends AppCompatActivity {
    DatabaseReference db;
    FirebaseHelperResto helper;
    MyAdapter adapter;
    RecyclerView rv;
    TextView name;
    ImageView image;
    ArrayList<Restaurant> arr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resto_list);

        //INITIALIZE RV
        rv= (RecyclerView) findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));


        //INITIALIZE FB
        db= FirebaseDatabase.getInstance().getReference();
        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Restaurant");//aferfer

        myRef.setValue("Hello, World!");
        myRef.addValueEventListener(new ValueEventListener() {//ewa
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d("", "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("", "Failed to read value.", error.toException());
            }
        });
        helper=new FirebaseHelperResto(db);

        //ADAPTER
        arr = new ArrayList<>();

      //  adapter=new MyAdapter(this,helper.retrieve());
        rv.setAdapter(adapter);

    }






}

