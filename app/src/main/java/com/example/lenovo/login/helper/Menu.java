package com.example.lenovo.login.helper;

public class Menu {
    private String menu;
    private int prix;
    public Menu(){
    }
    public Menu(String Name){ this.menu = Name;}

    public Menu(String Name,int prix){
        this.menu = Name;
        this.prix = prix;
    }
    public String getMenu() {
        return menu;
    }

    public int getPrix() {
        return prix;
    }

    public void setMenu(String name) {
        this.menu = name;
    }

    public void setPrix(int image) {
        this.prix = prix;
    }


    @Override
    public String toString() {
        return menu+" "+prix;
    }
}
