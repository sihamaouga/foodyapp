package com.example.lenovo.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Client_order extends AppCompatActivity {
        Button btnRsto ;
        Button out ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_order);


        btnRsto = findViewById(R.id.selectResto);
        out = findViewById(R.id.button5);

        Log.i("Smyti",""+Final.restoName);
        btnRsto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Client_order.this, Afficher_Resto.class);
                startActivity(intent);
            }
        });

        out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Client_order.this,MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
