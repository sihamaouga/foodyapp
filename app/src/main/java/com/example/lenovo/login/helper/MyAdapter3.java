package com.example.lenovo.login.helper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.example.lenovo.login.Cart;
import com.example.lenovo.login.Database.Database;
import com.example.lenovo.login.Interface.ItemClickListener;
import com.example.lenovo.login.Order;
import com.example.lenovo.login.R;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class MyAdapter3 extends RecyclerView.Adapter<MyViewHolderCart> {
    Context c;
    ArrayList<Order> spacecrafts;

    public MyAdapter3(Context c, ArrayList<Order> spacecrafts){
        this.c = c;
        this.spacecrafts = spacecrafts;

    }
    @NonNull
    @Override
    public MyViewHolderCart onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v=LayoutInflater.from(c).inflate(R.layout.cart,viewGroup,false);
        return new MyViewHolderCart(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderCart holder, int i) {
        TextDrawable drawable =TextDrawable.builder()
                .buildRound(""+spacecrafts.get(i).getQt(),Color.RED);
        holder.imgCartCount.setImageDrawable(drawable);
        Locale locale = new Locale("en","US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        int price = (Integer.parseInt(spacecrafts.get(i).getPrix()))*(Integer.parseInt(spacecrafts.get(i).getQt()));
        holder.txtPrice.setText(price+" DHS");
        holder.cartName.setText(spacecrafts.get(i).getMenu());
        holder.restocart.setText(spacecrafts.get(i).getResto());

      holder.setItemClickListener(new ItemClickListener() {
          @Override
          public void onClick(View view, int position, boolean isClick) {
      Order o = spacecrafts.get(position);
              new Database(c).deleteRow(o);
              Toast.makeText(c, "Ordre supprime", Toast.LENGTH_SHORT).show();
              Intent cartIntent = new Intent(c,Cart.class);
              c.startActivity(cartIntent);
          }
      });
    }

    @Override
    public int getItemCount() {

        return spacecrafts.size();
    }

}
