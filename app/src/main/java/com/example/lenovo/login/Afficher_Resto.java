package com.example.lenovo.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.login.helper.MyAdapter2;
import com.example.lenovo.login.helper.MyAdapter4;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Afficher_Resto extends AppCompatActivity {
    RecyclerView rv;
    MyAdapter4 adapter;
    ArrayList<RestoSide> liste;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afficher__resto);

        rv = findViewById(R.id.recyclerViewRstSide);
        rv.setLayoutManager(new LinearLayoutManager(this));
        liste = new ArrayList<>();

        String SERVER_URL = "http://10.50.17.218/food_App/restoSide.php";

        RequestQueue queue = Volley.newRequestQueue(Afficher_Resto.this);
        StringRequest request = new StringRequest(Request.Method.POST, SERVER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //    Toast.makeText(MenuList.this, "reponse"+response, Toast.LENGTH_SHORT).show();
                Log.i("My success",""+response);
                try {
                    //converting the string to json array object
                    JSONArray array = new JSONArray(response);

                    //traversing through all the object
                    for (int i = 0; i < array.length(); i++) {

                        //getting product object from json array
                        JSONObject product = array.getJSONObject(i);
                        String client = product.getString("client");
                        String adresse =product.getString("adresse");
                        String menu = product.getString("menu");
                        String qt = product.getString("qt");
                        String prix = product.getString("prix");

                        //adding the product to product list
                        liste.add(new RestoSide(client,adresse,menu,qt,prix));
                    }

                    //creating adapter object and setting it to recyclerview
                    adapter = new MyAdapter4(Afficher_Resto.this, liste);
                    rv.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Afficher_Resto.this, "my error :"+error, Toast.LENGTH_LONG).show();
                Log.i("My error",""+error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map = new HashMap<String, String>();
                map.put("restaurant",Final.restoName);



                return map;
            }
        };
        queue.add(request);
    }
}
