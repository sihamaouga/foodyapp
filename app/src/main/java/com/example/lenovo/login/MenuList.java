package com.example.lenovo.login;

import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.Constraints;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.lenovo.login.Database.Database;
import com.example.lenovo.login.helper.MyAdapter;
import com.example.lenovo.login.helper.MyAdapter2;
import com.google.gson.JsonSyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuList extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ElegantNumberButton numberButton ;
    ImageButton ib;

      RecyclerView rv2;
      MyAdapter2 adapter2;
      ArrayList<com.example.lenovo.login.helper.Menu> listMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // nom du resto
        final String nomResto= getIntent().getStringExtra("resto");


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //    Snackbar.make(view, nomResto, Snackbar.LENGTH_LONG)
              //          .setAction("Action", null).show();

                Intent cartIntent = new Intent(MenuList.this,Cart.class);
                startActivity(cartIntent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // create recycler view
        rv2 = (RecyclerView) findViewById(R.id.recyclerView2);
        rv2.setLayoutManager(new LinearLayoutManager(this));
        listMenu = new ArrayList<>();

        String SERVER_URL = "http://10.50.17.218/food_App/getmenu.php";
        // appel la fonction
       // LongOperation2 longOperation =   new LongOperation2();
   //     longOperation.execute(SERVER_URL);

      //  new PostDataAsyncTask().execute();
      //  String nomResto= getIntent().getStringExtra("resto");
       // Log.e("nom resto apres envoi",nomResto);


        RequestQueue queue = Volley.newRequestQueue(MenuList.this);
        StringRequest request = new StringRequest(Request.Method.POST, SERVER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            //    Toast.makeText(MenuList.this, "reponse"+response, Toast.LENGTH_SHORT).show();
                Log.i("My success",""+response);
                try {
                    //converting the string to json array object
                    JSONArray array = new JSONArray(response);

                    //traversing through all the object
                    for (int i = 0; i < array.length(); i++) {

                        //getting product object from json array
                        JSONObject product = array.getJSONObject(i);
                  String menu = product.getString("nom");
                  int prix = product.getInt("prix");
                        //adding the product to product list
                        listMenu.add(new com.example.lenovo.login.helper.Menu(menu,prix));
                    }

                    //creating adapter object and setting it to recyclerview
                    adapter2 = new MyAdapter2(MenuList.this, listMenu,nomResto);
                    rv2.setAdapter(adapter2);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(MenuList.this, "my error :"+error, Toast.LENGTH_LONG).show();
                Log.i("My error",""+error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> map = new HashMap<String, String>();
                map.put("resto",nomResto);



                return map;
            }
        };
        queue.add(request);





        //// cart traitement





    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Restaurants) {
            Intent intent = new Intent(MenuList.this, RestoList2.class);
            startActivity(intent);// send to restolist 3
            finish();
            // Handle the camera action
        } else if (id == R.id.nav_cart) {
            Intent cartIntent = new Intent(MenuList.this,Cart.class);
            startActivity(cartIntent);

        } else if (id == R.id.signout) {
            Intent intent = new Intent(MenuList.this,MainActivity.class);
            startActivity(intent);// send to restolist 3
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private class LongOperation2 extends AsyncTask<String, Void, Void> {
        private String jsonResponse; public ArrayList<String> liste;

        protected void onPreExecute() {
            super.onPreExecute();
        }

        // do In bg
        @Override
        protected Void doInBackground(String... strings) {

            liste = new ArrayList<>();
            try {
                URL url = new URL(strings[0]);  //argument supplied in the call to AsyncTask
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("User-Agent", "");
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.connect();
                InputStream isResponse = urlConnection.getInputStream();
                BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(isResponse));
                String myLine = "";
                StringBuilder strBuilder = new StringBuilder();
                while ((myLine = responseBuffer.readLine()) != null) {
                    strBuilder.append(myLine);
                    jsonResponse = strBuilder.toString();
                    Log.e("RESPONSE", jsonResponse);
                    //try 2
                    try {
                        JSONArray resto = new JSONArray(jsonResponse);

                        for(int i=0;i<resto.length();i++){
                            JSONObject restobject = resto.getJSONObject(i);
                            String menu = restobject.getString("nom");
                         //   int img = restobject.getInt("image");
                            Log.e("nom",menu);
                           // Restaurant restaurant = new Restaurant(nom,img);
                            liste.add(menu);
                        }

                        Log.e("la liste",liste.toString());
                  //      adapter2 = new MyAdapter2(MenuList.this, liste);
                        rv2.setAdapter(adapter2);

                    }catch (JSONException e){}
                }
            } catch (Exception e) {};

            return null;
        }

        protected void onPostExecute(Void unused) {
            try
            {
                //     Gson gson = new Gson();
                //     Type listType = new TypeToken<ArrayList<Restaurant>>() {
                //    }.getType();
                //    arr = gson.fromJson(jsonResponse, listType);
            } catch(JsonSyntaxException e) { }
        }
    }
}
class PostDataAsyncTask extends AsyncTask<String, String, String> {

    protected void onPreExecute() {
        super.onPreExecute();
        // do stuff before posting data
    }

    @Override
    protected String doInBackground(String... strings) {
        try {

            // 1 = post text data, 2 = post file
            int actionChoice = 1;

            // post a text data

                postText();


            // post a file


        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String lenghtOfFile) {
        // do stuff after posting data
    }


    // this will post our text data
    private void postText() {
        try {
            // url where the data will be posted
            String postReceiverUrl = "http://192.168.1.105/food_App/senddata.php";
            Log.v(Constraints.TAG, "postURL: " + postReceiverUrl);

            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();

            // post header
            HttpPost httpPost = new HttpPost(postReceiverUrl);

            // add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("firstname", "Mike"));
            nameValuePairs.add(new BasicNameValuePair("lastname", "Dalisay"));
            nameValuePairs.add(new BasicNameValuePair("email", "mike@testmail.com"));

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // execute HTTP post request
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {

                String responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(ContentValues.TAG, "Response: " + responseStr);

                // you can add an if statement here and do other actions based on the response
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // will post our text file

}