package com.example.lenovo.login;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.textclassifier.TextLinks;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.login.helper.MyAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
public class RestoList2 extends AppCompatActivity {
    RecyclerView rv;
    ArrayList<Restaurant> arr ;
    MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resto_list);

        // create recycler view
        rv = (RecyclerView) findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));


    //  Log.e("client",email);
        // make url and execute server
        String SERVER_URL = "http://10.50.17.218/food_App/getdata.php";
        // appel la fonction
         LongOperation longOperation =   new LongOperation();
        longOperation.execute(SERVER_URL);

       //  arr = new ArrayList<>();
       //  arr = longOperation.liste;
       //   Log.e("array",arr.toString());
        // add adapter
        //  adapter = new MyAdapter(this, arr);
        //   rv.setAdapter(adapter);
    }

    private class LongOperation extends AsyncTask<String, Void, Void> {
        private String jsonResponse; public ArrayList<Restaurant> liste;

        protected void onPreExecute() {
            super.onPreExecute();
                                      }

        // do In bg
        @Override
        protected Void doInBackground(String... strings) {

            liste = new ArrayList<>();
            try {
                URL url = new URL(strings[0]);  //argument supplied in the call to AsyncTask
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("User-Agent", "");
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.connect();
                InputStream isResponse = urlConnection.getInputStream();
                BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(isResponse));
                String myLine = "";
                StringBuilder strBuilder = new StringBuilder();
                while ((myLine = responseBuffer.readLine()) != null) {
                    strBuilder.append(myLine);
                    jsonResponse = strBuilder.toString();
                    Log.e("RESPONSE", jsonResponse);
                    //try 2
                    try {
                        JSONArray resto = new JSONArray(jsonResponse);

                        for(int i=0;i<resto.length();i++){
                            JSONObject restobject = resto.getJSONObject(i);
                            String nom = restobject.getString("nom");
                            int img = restobject.getInt("image");
                            Log.e("nom",nom);
                            Restaurant restaurant = new Restaurant(nom,img);
                            liste.add(restaurant);
                                                          }

                    Log.e("la liste",liste.toString());
                        adapter = new MyAdapter(RestoList2.this, liste);
                        rv.setAdapter(adapter);

                    }catch (JSONException e){}
                                                                      }
                             } catch (Exception e) {};

                                   return null;
                                                          }

        protected void onPostExecute(Void unused) {
            try
            {
           //     Gson gson = new Gson();
           //     Type listType = new TypeToken<ArrayList<Restaurant>>() {
            //    }.getType();
            //    arr = gson.fromJson(jsonResponse, listType);
            } catch(JsonSyntaxException e) { }
                                                  }
                                                                        }
}
