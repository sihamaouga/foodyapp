package com.example.lenovo.login.helper;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.login.Interface.ItemClickListener;
import com.example.lenovo.login.R;
public class MyViewHolderResto extends RecyclerView.ViewHolder implements View.OnClickListener{
    TextView name;
    ImageView image;
    private ItemClickListener itemClickListener;
    public MyViewHolderResto(View itemView) {
        super(itemView);
        name=(TextView) itemView.findViewById(R.id.texte);
        image=(ImageView) itemView.findViewById(R.id.imageur);
        itemView.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
        }

    @Override
    public void onClick(View v) {
    itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}
