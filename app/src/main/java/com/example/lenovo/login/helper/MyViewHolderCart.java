package com.example.lenovo.login.helper;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.login.Interface.ItemClickListener;
import com.example.lenovo.login.R;

public class MyViewHolderCart extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,View.OnClickListener {
    public TextView cartName,txtPrice,restocart;
    public ImageView imgCartCount ;
    CardView cardView;
    private ItemClickListener itemClickListener;

    public void setCartName(TextView cartName){
        this.cartName = cartName;
    }

    public MyViewHolderCart(@NonNull View itemView) {
        super(itemView);
        cartName = (TextView)itemView.findViewById(R.id.cart_item_name);
        txtPrice = (TextView)itemView.findViewById(R.id.cart_item_Price);
        restocart = (TextView) itemView.findViewById(R.id.cart_item_resto);
        imgCartCount = (ImageView) itemView.findViewById(R.id.cart_item_count);
        cardView = (CardView)itemView.findViewById(R.id.cardview);
        imgCartCount.setOnCreateContextMenuListener(this);

       itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("Select The Action");
    }


    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}
