package com.example.lenovo.login.helper;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.lenovo.login.Database.Database;
import com.example.lenovo.login.Interface.ItemClickListener;
import com.example.lenovo.login.MenuList;
import com.example.lenovo.login.Order;
import com.example.lenovo.login.R;
import com.example.lenovo.login.Restaurant;

import java.util.ArrayList;

public class MyAdapter2 extends RecyclerView.Adapter<MyViewHoldeMenu>{
    Context c;
    ArrayList<Menu> spacecrafts;
    String nomResto;
    String menu;
    int prix;
  public MyAdapter2(Context c, ArrayList<Menu> spacecrafts,String nomResto){
      this.c = c;
      this.spacecrafts = spacecrafts;
      this.nomResto = nomResto;
  }
    @NonNull
    @Override
    public MyViewHoldeMenu onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v=LayoutInflater.from(c).inflate(R.layout.food_details,viewGroup,false);
        return new  MyViewHoldeMenu(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHoldeMenu holdeMenu, int i) {
      Menu m = spacecrafts.get(i);
      menu = m.getMenu();
      prix = m.getPrix();
        holdeMenu.menu.setText(m.getMenu());
        holdeMenu.price.setText(m.getPrix()+" DHS");
        holdeMenu.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isClick) {

                int n = Integer.parseInt(holdeMenu.numberButton.getNumber());
                if(n!=0) {
                //    Toast.makeText(c,"Vous avez choisi "+spacecrafts.get(position),Toast.LENGTH_SHORT).show();
                    new Database(c).addToCart(new Order(nomResto, spacecrafts.get(position).getMenu(), holdeMenu.numberButton.getNumber(), spacecrafts.get(position).getPrix() + "", ""));
                    Toast.makeText(c, "Ajouter au panier "+spacecrafts.get(position), Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return spacecrafts.size();
    }
}
