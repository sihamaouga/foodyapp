package com.example.lenovo.login.Interface;

import android.view.View;

public interface ItemClickListener {
    void onClick(View  view, int position, boolean isClick);
}
