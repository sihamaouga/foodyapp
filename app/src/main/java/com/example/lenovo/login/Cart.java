package com.example.lenovo.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.lenovo.login.Database.Database;
import com.example.lenovo.login.helper.MyAdapter3;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Cart extends AppCompatActivity {
   RecyclerView rcv ;
   RecyclerView.LayoutManager layoutManager;
   Button btnPlace;
   TextView total;
   ArrayList<Order> carts = new ArrayList<>();
   MyAdapter3 adapter3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);


        //init
        rcv = (RecyclerView) findViewById(R.id.listCart);
        rcv.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rcv.setLayoutManager(layoutManager);
        total = (TextView) findViewById(R.id.total);
        btnPlace = (Button) findViewById(R.id.placeOrder);

        LoadListFood();

        btnPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           //  new Database(Cart.this).cleanCart();
           //     Intent cartIntent = new Intent(Cart.this,Cart.class);
           //     startActivity(cartIntent);
            showAlertDialog();
            }
        });


    }

    private void LoadListFood() {
      carts =  new Database(this).getCarts();

      Log.e("sqlite",carts.toString());
      adapter3 = new MyAdapter3(this,carts);
      rcv.setAdapter(adapter3);

      //calculer total price
        int toto = 0 ;
        for(Order order : carts){
            toto += (Integer.parseInt(order.getPrix()))*(Integer.parseInt(order.getQt()));

        }
          total.setText(" "+toto+" DHS");
    }
  public void showAlertDialog(){

      AlertDialog.Builder alertDialog = new AlertDialog.Builder(Cart.this);
       alertDialog.setTitle("One more step");
       alertDialog.setMessage("Save your Location");
       alertDialog.setIcon(R.drawable.ic_add_location_black_24dp);
       alertDialog.setPositiveButton("Save", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {
               Intent MapIntent = new Intent(Cart.this,MapsActivity.class);
               Final.userCarts = carts;
               startActivity(MapIntent);
           }
       });

       if(!carts.isEmpty()) {
           alertDialog.show();
       }
  }
}
