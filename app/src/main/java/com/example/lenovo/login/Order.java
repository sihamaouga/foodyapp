package com.example.lenovo.login;

public class Order {
    private String resto;
    private String menu;
    private String qt;
    private String prix;
    private String discount;

    public Order(String resto,String menu,String qt,String prix,String discount){
        this.resto = resto;
        this.menu = menu;
        this.qt = qt;
        this.prix = prix;
        this.discount = discount;


    }

    public String getResto() {
        return resto;
    }

    public void setResto(String resto) {
        this.resto = resto;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getQt() {
        return qt;
    }

    public void setQt(String qt) {
        this.qt = qt;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
    public String toString(){
        return resto+" "+menu+" "+qt+" "+prix+" "+discount+" ";
    }
}
